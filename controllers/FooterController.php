<?php 

/**
 * Footer Controller Class
 */
class FooterController extends Controller
{
    public function init()
    {     
        return $this->load->view('common/footer');
    }
}