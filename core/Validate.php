<?php 

/**
 * Validate
 */
class Validate
{   
    public function email($email)
    {
        if (empty($email)) {
            return 'empty';
        } else {
            return filter_var($email, FILTER_VALIDATE_EMAIL);
        } 
    }

    public function int($int)
    {
        return filter_var($int, FILTER_VALIDATE_INT);
    }

    public function url($url)
    {
        return filter_var($url, FILTER_VALIDATE_URL);
    }

    public function string($string)
    {
        if ($string != strip_tags($string)) {
            return false;
        }

        return $string;
    }

    public function specChars($specChars)
    {
        if (preg_match('/[^a-zA-Z0-9]/', trim($specChars))) {
            return false;
        } else {
            return $specChars;
        }
    }

    public function checkEmpty()
    {
        
    }
}