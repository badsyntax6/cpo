var Lister = new Object();

/**
 * Lister Initializer
 *
 * This is the primary funciton for this object. It will set the properties and call all other functions.
 */
Lister.init = function(admin) {
    Lister.url = window.location.pathname.split('/');
    Lister.view = Lister.url[1];
    Lister.params = {orderby: 'id', direction: 'asc', page: '1', limit: '15'};

    console.log('/' + Lister.view + '/drawTable');
    
    Lister.drawTable();
    Lister.goToPage();
    Lister.goPrev();
    Lister.goNext();
    Lister.limitPageRecords();
    Lister.sortRecords();
    Lister.checkAll();
    Lister.enableControls();
    Lister.highlightRow();
}

/**
 * Draw Table
 * 
 * This function is responsible for drawing a table of records in the list view. This function will also 
 * call drawPagination() and getTotalRecords().
 */
Lister.drawTable = function() {
    $.ajax({
        url: '/' + Lister.view + '/drawTable',
        type: 'post',
        data: Lister.params,
        beforeSend: function() {
            $('.loading').show();
        },
        success: function(response, status, xhr) {
            // $('body').prepend(response);
            if ($.trim(response)) {
                var data = JSON.parse(response);
                // Draw the table
                $('.panel-content').html(data.table);
                // Number each row
                $('.data-row').each(function(index, item) {
                    var start_num = data.start;
                    var num = parseInt(index) + parseInt(start_num);
                    num++;
                    $(this).find('.number-col').text(num);               
                });

                Lister.drawPagination();
                Lister.getTotalRecords();
            } 
        },
        complete: function() {
            $('.loading').fadeOut(500);
        }
    });
}

/**
 * Draw Pagination
 * 
 * This function will create pagination links for the table list.
 */
Lister.drawPagination = function() {
    $('.pagination').html('');
    $.ajax({
        url: '/list/getPageTotal/' + Lister.view,
        type: 'post',
        data: Lister.params,
        success: function(response, status, xhr) {
            if ($.trim(response)) {
                // Add previous button if page is not first page.
                if (Lister.params.page != 1) {
                    $('.pagination').prepend('<button type="button" class="btn-prev"><i class="fas fa-caret-left fa-fw"></i></button><hr class="divider">');
                }
                // Add page links with page numbers.
                for (var i = 1; i <= response; i++) {
                    $('.pagination').append('<button type="button" class="btn-page">' + i + '</button><hr class="divider">');
                }
                // Add next button if page is not last page.
                if (Lister.params.page != response) {
                    $('.pagination').append('<button class="btn-next"><i class="fas fa-caret-right fa-fw"></i></button>');
                }
                // Highlight current page in pagination nav.
                $('.pagination button').each(function() {
                    if ($(this).text() == Lister.params.page) {
                        $(this).css({'box-shadow' : 'inset 0px 0px 3px 1px rgba(0, 0, 0, 0.1)', 'color' : '#aaa'});
                    }
                });
            } 
        }
    });
}

/**
 * Get Total Records
 * 
 * This function will get the total number of records and display the total at the bottom of the list.
 */
Lister.getTotalRecords = function() {
    $.ajax({
        url: '/list/getTotalRecordsNumber/' + Lister.view,
        type: 'post',
        data: Lister.params,
        success: function(response, status, xhr) {
            if ($.trim(response)) {
                $('.total').text('Total: ' + response);
            } 
        }
    });
}

/**
 * Go to Page
 *
 * This function will re-draw the table starting at the page that is selected in the pagination. 
 */
Lister.goToPage = function() {
    $('body').on('click', '.btn-page', function() {
        Lister.params.page = $(this).text();
        Lister.drawTable();
    });
}

/**
 * Go Previous Page
 * 
 * This function will re-draw the table one page down from the current page.
 */
Lister.goPrev = function() {
    $('body').on('click', '.btn-prev', function() {
        Lister.params.page = --Lister.params.page;
        Lister.drawTable();
    });
}

/**
 * Go Next Page
 * 
 * This function will re-draw the table one page up from the current page.
 */
Lister.goNext = function() {
    $('body').on('click', '.btn-next', function() {
        Lister.params.page = ++Lister.params.page;
        Lister.drawTable();
    });
}

/**
 * Limit Page Records
 * 
 * This function will limit the amount of records that are displayed in the table list.
 */
Lister.limitPageRecords = function() {
    // Add selected prop to current page limit in select menu
    $('.limit' + Lister.params.limit).prop('selected', true);

    // Choose max records to show per page.
    $('.limit-per-page').on('change', function() {
        $('.limit-per-page option').each(function() {
            if ($(this).prop('selected') == true) {
                Lister.params.limit = $(this).text();
                Lister.params.page = 1;
                Lister.drawTable();
            }
        });
    });
}

/**
 * Sort Records
 * 
 * This function sorts the table when the user clicks a link in the table header.
 */
Lister.sortRecords = function() {
    $('body').on('click', '.btn-sort', function() {
        // Set the order by the table header text.
        Lister.params.orderby = $(this).text().toLowerCase().replace(/ /g, '_');
        // Determine the direction the table should sort by.
        if (Lister.params.direction === 'asc') {
            Lister.params.direction = 'desc';
        } else {
            Lister.params.direction = 'asc';
        }
        Lister.drawTable();
    });
}

/**
 * Check All
 * 
 * Check all checkboxes if the #check-all checkbox is checked.
 */
Lister.checkAll = function() {
    $('body').on('click', '#check-all', function() {
        if ($('#check-all').prop('checked') == true) {
            $('.checkbox').prop('checked', true).change();
        }
        if ($('#check-all').prop('checked') == false) {
            $('.checkbox').prop('checked', false).change();
        }
    }); 
}

/**
 * Enable List Controls
 * 
 * This function will enable the master list controls. These buttons will be disabled by default but visible in 
 * the list view. This function listen for a checkbox to change. When it does it will  enable the list controls. 
 * These controls are different from the pagination and limit controls.
 */
Lister.enableControls = function() {
    $('body').on('change', '.checkbox', function() {
        if ($('input:checkbox:checked').length) {
            $('.btn-list-control').prop('disabled', false);
        }
        if (!$('input:checkbox:checked').length) {
            $('.btn-list-control').prop('disabled', true);
        }
    });  
}

/**
 * Highlight Row
 * 
 * Whenever a row and an element inside a row is clicked this function will highlight that row by changing its 
 * background color.
 */
Lister.highlightRow = function() {
    $('body').on('click', '.data-row', function() {
        $('tr').removeAttr('style');
        $('.data-row').removeClass('tr-highlight');
        $(this).addClass('tr-highlight');
    });
}