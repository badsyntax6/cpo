<?php 

/**
 * Home Controller Class
 */
class HomeController extends Controller
{
    /**
     * Init method
     *
     * The init methods in controller classes will be called automatically when a 
     * controller is loaded. 
     *
     * Routes
     * - http://root/home
     * - http://root/home/init
     *
     * The HomeController class is the default controller for the application. 
     * This means that invalid routes will default to this init method.
     */
    public function init()
    {   
        $users = $this->getUserInfo();
        $settings = $this->getSettingsInfo();

        foreach ($users as $key => $value) {
            $view[$key] = $value;
        }

        foreach ($settings as $key => $value) {
            $view[$key] = $value;
        }

        $view['logs'] = [];
        $logs = $this->load->model('log')->getLogs();

        if (!empty($logs)) {
            foreach ($logs as $log) {
                $view['logs'][] = [
                    'id' => $log['id'],
                    'time' => date('d/m/Y h:ia', strtotime($log['time'])),
                    'event' => $log['event']
                ];
            }
        } 

        $view['errors'] = [];
        $errors = $this->load->model('log')->getErrors();

        if (!empty($errors)) {
            foreach ($errors as $error) {
                $view['errors'][] = [
                    'id' => $error['id'],
                    'time' => date('d/m/Y h:ia', strtotime($error['time'])),
                    'event' => $error['event']
                ];
            }
        } 

        $view['header'] = $this->load->controller('header')->init();
        $view['footer'] = $this->load->controller('footer')->init();
        $view['search'] = $this->load->controller('search')->init();
        $view['nav'] = $this->load->controller('navigation')->init();
        $view['breadcrumb'] = $this->load->controller('breadcrumb')->init();

        exit($this->load->view('common/home', $view));
    }

    private function getUserInfo()
    {
        $model = $this->load->model('users');
        $latest = $model->getLatestRegistrant();
        $total_users = $model->getTotalUsersNumber();
        $registered = $model->getUsersByGroup(2);
        $mods = $model->getUsersByGroup(3);
        $admins = $model->getUsersByGroup(4);
        $banned = $model->getUsersByGroup(0);

        $data['total_users'] = $total_users ? $total_users : 0;
        $data['registered'] = $registered ? $registered : 0;
        $data['mods'] = $mods ? $mods : 0;
        $data['admins'] = $admins ? $admins : 0;
        $data['banned'] = $banned ? $banned : 0;
        $data['latest_reg'] = $latest['username'];

        return $data;
    }

    private function getSettingsInfo()
    {
        $model = $this->load->model('settings');
        $maintenance = $this->load->model('settings')->getSetting('maintenance_mode');
        $lang = $this->load->model('settings')->getSetting('language');

        $data['sitename'] = $this->load->model('settings')->getSetting('sitename');
        $data['language'] = ucfirst($lang);
        $data['theme'] = $this->load->model('settings')->getSetting('theme');
        $data['maintenance'] = $maintenance == 1 ? 'On' : 'Off';

        return $data;
    }

    public function clearLog()
    {
        if ($this->load->model('log')->clearLog()) {
            $output['alert'] = 'success';
            $output['message'] = $this->language->get('settings/logs_cleared');
            $this->output->json($output, 'exit');
        }
    }

    public function clearErrors()
    {
        if ($this->load->model('log')->clearErrors()) {
            $output['alert'] = 'success';
            $output['message'] = $this->language->get('settings/errors_cleared');
            $this->output->json($output, 'exit');
        }
    }
}