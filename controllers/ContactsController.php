<?php

class ContactsController extends Controller
{
    public function init($view = 'list')
    {
        if ($view == 'list') {
            exit($this->load->controller('list')->drawList('contacts'));
        }

        $this->drawUser($view);
    }

    public function drawTable()
    {
        $paginated = $this->load->model('pagination')->paginate('contacts', $_POST['orderby'], $_POST['direction'], $_POST['page'], $_POST['limit']);

        foreach ($paginated['list'] as $user) {
            switch ($user['group']) {
                case '2':
                    $group = 'Registered';
                    break;
                case '3':
                    $group = 'Moderator';
                    break;
                case '4':
                    $group = 'Admin';
                    break;
                case '0':
                    $group = 'Locked';
                    break;
                default:
                    $group = 'Activation pending';
                    break;
            }
            
            $view['contacts'][] = [
                'id' => $user['id'],
                'firstname' => $user['firstname'],
                'lastname' => $user['lastname'],
                'username' => $user['username'],
                'email' => $user['email'],
                'signup_date' => date('d M, Y', strtotime($user['signup_date'])),
                'status' => $this->userOnline($user['username']) ? 'Online' : 'Offline',
                'group' => $group,
                'group_num' => $user['group']
            ];
        }

        $output = [
            'table' => $this->load->view('contacts/list', $view), 
            'start' => $paginated['start']
        ];

        $this->output->json($output, 'exit');
    }

    public function new()
    {
        $view['header'] = $this->load->controller('header')->init();
        $view['footer'] = $this->load->controller('footer')->init();
        $view['nav'] = $this->load->controller('navigation')->init();
        $view['breadcrumb'] = $this->load->controller('breadcrumb')->init();

        exit($this->load->view('contacts/new', $view));
    }

    public function save()
    {
        // $admin = $this->logged_user->user_id;

        $post = [];

        foreach ($_POST as $key => $value) {
            $post[$key] = trim($value);
        } 

        $this->validateName($post['name']);
        $this->validateEmail($post['email']);
    }

    private function validateName($name)
    {
        // Remove unwanted characters
        if (empty($name)) {
            $output = ['alert' => 'error', 'message' => $this->language->get('clients/name_empty')];
            $this->output->json($output, 'exit');
        }

        $this->name = preg_replace('/[^A-Za-z0-9_-]/', '', $name);

        // If name is greater than 20 chars, exit with error.
        if (strlen($this->name) > 20) {
            $output = ['alert' => 'error', 'message' => $this->language->get('clients/name_invalid')];
            $this->output->json($output, 'exit');
        }
    }

    private function validatePhone($phone)
    {
        // Remove unwanted characters
        if (empty($phone)) {
            $output = ['alert' => 'error', 'message' => $this->language->get('clients/phone_empty')];
            $this->output->json($output, 'exit');
        }

        if (preg_match("/[a-z]/i", $phone)){
            $output = ['alert' => 'error', 'message' => $this->language->get('clients/phone_letters')];
            $this->output->json($output, 'exit');
        }

        // If name is greater than 20 chars, exit with error.
        if (strlen($this->phone) > 20) {
            $output = ['alert' => 'error', 'message' => $this->language->get('clients/phone')];
            $this->output->json($output, 'exit');
        }
    }

    private function validateEmail($email)
    {
        // Sanitize and store the email in a property.
        if (empty($email)) {
            $output = ['alert' => 'error', 'message' => $this->language->get('signup/email_empty')];
            $this->output->json($output, 'exit');
        }

        $this->email = $this->sanitize->email($email);

        if (!$this->validate->email($this->email)) {
            $output = ['alert' => 'error', 'message' => $this->language->get('signup/email_invalid')];
            $this->output->json($output, 'exit');
        }

        // If a matching email is found, exit with an error.
        if ($this->user_model->getUser('email', $this->email)) {
            $output = ['alert' => 'error', 'message' => $this->language->get('signup/email_taken')];
            $this->output->json($output, 'exit');
        }
    }
}
