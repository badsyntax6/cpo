<?php 

/**
 * Clients Model Class
 *
 * Interact with the database to process data related to the clients.
 */
class ClientsModel extends Model
{
    /**
     * Get all user data
     *
     * Get all clients data and return their data in arrays.
     * @return array
     */
    public function getClients()
    {
        // SELECT * FROM `clients`
        $select = $this->table('clients')->select('*')->getAll();
        if ($select) {
            if ($select['status'] == 'success') {
                return empty($select['response']) ? false : $select['response'];
            } else {
                return false;
            }
        }
    }

    /**
     * Get a single clients data
     *
     * Get a clients data and return their data in an array.
     * @param $param
     * @param $data  
     * @return array
     */
    public function getClient($param, $data)
    {
        // SELECT * FROM `clients` WHERE `id` = "2"
        $select = $this->table('clients')->select('*')->where($param, $data)->get();
        if ($select) {
            if ($select['status'] == 'success') {
                return empty($select['response']) ? false : $select['response'];
            } else {
                return false;
            }
        }
    }

    /**
     * Insert user into database
     *
     * Insert a new user record into the database.
     * @param array $post      
     * @return bool   
     */
    public function insertClient($post)
    {
        // INSERT INTO `clients` (`username`, `email`, `password`, `key`, `last_active`, `ip`) VALUES (?, ?, ?, ?, ?, ?)
        $insert = $this->table('clients')->insert($post)->execute();
        if ($insert) {
            if ($insert['status'] == 'success') {
                return empty($insert['response']) ? true : $insert['response'];
            } else {
                return false;
            }
        }
    }

    /**
     * Update a user record
     *
     * @param mixed $data   
     * @return mixed         
     */
    public function updateClient($data)
    {
        // UPDATE `clients` SET `username` = ? WHERE `id` = ?
        $update = $this->table('clients')->update($data)->where('id')->execute();
        if ($update) {
            if ($update['status'] == 'success') {
                if ($update['affected_rows'] > 0) {
                    return empty($update['response']) ? true : $update['response'];
                } else {
                    return false;
                }
            } else {
                return false;
            }
        }
    }

    /**
     * Insert recovery link
     *
     * This method inserts a recovery link into the database. It expects a
     * email parameter and a token parameter.
     * @param string $email
     * @param string $token    
     */
    public function insertResetLink($data)
    {
        // INSERT INTO `reset_links` (`email`, `token`) VALUES (?, ?)
        $insert = $this->table('reset_links')->insert($data)->execute();
        if ($insert) {
            if ($insert['status'] == 'success') {
                return empty($insert['response']) ? true : $insert['response'];
            } else {
                return false;
            }
        }
    }

    public function getRecoveryLink($token)
    {
        // SELECT * FROM `reset_links` WHERE `token` = "76e8ed291b483ab7ac87177" LIMIT 1
        $link = $this->table('reset_links')->select('*')->where('token', $token)->limit(1)->get();
        if ($link) {
            if ($link['status'] == 'success') {
                return empty($link['response']) ? false : $link['response'];
            } else {
                return false;
            }
        }
    }

    public function deleteRecoveryLink($token)
    {
        // DELETE FROM `reset_links` WHERE `token` = ?
        $delete = $this->table('reset_links')->delete()->where('token', $token)->execute();
        if ($delete) {
            if ($delete['status'] == 'success') {
                return empty($delete['response']) ? true : $delete['response'];
            } else {
                return false;
            }
        }
    }

    public function getCountries()
    {
        // SELECT * FROM `countries`
        $select = $this->table('countries')->select('*')->getAll();
        if ($select) {
            if ($select['status'] == 'success') {
                return empty($select['response']) ? true : $select['response'];
            } else {
                return false;
            }
        }
    }

    public function getLoginAttempts($data)
    {
        // SELECT * FROM `login_attempts` WHERE `ip` = "192.168.1.1"
        $select = $this->table('login_attempts')->select('*')->where('ip', $data)->get();
        if ($select) {
            if ($select['status'] == 'success') {
                return empty($select['response']) ? false : $select['response'];
            } else {
                return false;
            }
        }
    }

    public function insertLoginAttempt($data)
    {
        // INSERT INTO `login_attempts` (`email`, `ip`, `lock_time`, `attempts`) VALUES (?, ?, ?, ?)
        $insert = $this->table('login_attempts')->insert($data)->execute();
        if ($insert) {
            if ($insert['status'] == 'success') {
                return empty($insert['response']) ? true : $insert['response'];
            } else {
                return false;
            }
        }
    }

    public function updateLoginAttempts($data)
    {
        $update = $this->table('login_attempts')->update($data)->where('ip')->execute();
        if ($update) {
            if ($update['status'] == 'success') {
                if ($update['affected_rows'] > 0) {
                    return empty($update['response']) ? true : $update['response'];
                } else {
                    return false;
                }
            } else {
                return false;
            }
        }
    }

    public function deleteLoginAttempts($email)
    {
        // DELETE FROM `login_attempts` WHERE `email` = 'person@place.com'
        $delete = $this->table('login_attempts')->delete()->where('email', $email)->execute();
        if ($delete) {
            if ($delete['status'] == 'success') {
                return empty($delete[1]) ? true : $delete[1];
            } else {
                return false;
            }
        }
    }

    public function getTotalClientsNumber()
    {
        // SELECT * FROM `clients`
        $select = $this->table('clients')->select('*')->getTotal();
        if ($select) {
            if ($select['status'] == 'success') {
                return empty($select['response']) ? false : $select['response'];
            } else {
                return false;
            }
        }
    }

    public function getClientsByGroup($data)
    {
        // SELECT * FROM `clients` WHERE `id` = ?
        $select = $this->table('clients')->select('*')->where('group', $data)->getTotal();
        if ($select) {
            if ($select['status'] == 'success') {
                return empty($select['response']) ? false : $select['response'];
            } else {
                return false;
            }
        }
    }

    public function getLatestRegistrant()
    {
        // SELECT * FROM `clients` ORDER BY `id` DESC LIMIT 1
        $select = $this->table('clients')->select('username, signup_date')->orderby('id', 'desc')->limit(1)->get();
        if ($select) {
            if ($select['status'] == 'success') {
                return empty($select['response']) ? false : $select['response'];
            } else {
                return false;
            }
        }
    }

    public function deleteClient($id)
    {
        // DELETE FROM `clients` WHERE `id` = ?
        $delete = $this->table('clients')->delete()->where('id', $id)->execute();
        if ($delete) {
            return true;
        } else {
            return false;
        }
    }
}