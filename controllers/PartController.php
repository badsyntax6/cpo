<?php 

/**
 * Part Controller Class
 */
class PartController extends Controller
{
    /**
     * Init method
     *
     * The init methods in controller classes will be called automatically when a 
     * controller is loaded. 
     *
     * Routes
     * - http://root/part
     * - http://root/part/init
     *
     * The HomeController class is the default controller for the application. 
     * This means that invalid routes will default to this init method.
     */
    public function init()
    {   
        $view['header'] = $this->load->controller('header')->init('Home');
        $view['footer'] = $this->load->controller('footer')->init();

        exit($this->load->view('common/home', $view));
    }

    public function one()
    {
        $view['header'] = $this->load->controller('header')->init('Home');
        $view['footer'] = $this->load->controller('footer')->init();
        $view['nav'] = $this->load->controller('navigation')->init();

        exit($this->load->view('part/one', $view));
    }
}