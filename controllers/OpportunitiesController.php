<?php

class OpportunitiesController extends Controller
{
    public function leads()
    {
        $view['header'] = $this->load->controller('header')->init();
        $view['footer'] = $this->load->controller('footer')->init();
        $view['nav'] = $this->load->controller('navigation')->init();
        $view['breadcrumb'] = $this->load->controller('breadcrumb')->init();

        exit($this->load->view('opportunities/leads', $view));
    }
}
