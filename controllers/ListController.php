<?php 

/**
 * List Controller Class
 */
class ListController extends Controller
{
    public function drawList($controls_type) 
    {
        $settings_controller = $this->load->controller('settings');
        $menu_setting = $settings_controller->getMenuSetting();

        $view['header'] = $this->load->controller('header')->init();
        $view['footer'] = $this->load->controller('footer')->init();
        $view['search'] = $this->load->controller('search')->init();
        $view['menu_class'] = $menu_setting ? 'menu-open' : '';
        $view['nav'] = $this->load->controller('navigation')->init();
        $view['breadcrumb'] = $this->load->controller('breadcrumb')->init();
        $view['controls'] = $this->load->view($controls_type . '/controls');

        return $this->load->view('utilities/list', $view);
    }

    public function getPageTotal($table = null)
    {
        $orderby = $this->load->model('pagination')->checkOrderby($_POST['orderby']);
        $paginated = $this->load->model('pagination')->paginate($table, $orderby, $_POST['direction'], $_POST['page'], $_POST['limit']);

        $this->output->text($paginated['pages']);
    }

    public function getTotalRecordsNumber($table)
    {
        $output = $this->load->model('pagination')->getTotalRecordsNumber($table);
        $this->output->text($output);
    }
}