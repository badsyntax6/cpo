<?php 

// Start a new session.
if (!isset($_SESSION)) { session_start(); }

// Define Gusto version.
define('VERSION', '1.0.0');
define('BUILD', '19.1.30-09.18.25');

// Include Gusto config.
require_once str_replace('\\', '/', $_SERVER['DOCUMENT_ROOT']) . '/config/config.php';

// Include start files.
require_once ROOT_DIR . '/autoload.php';
require_once ROOT_DIR . '/startup.php';

// Autoload.
spl_autoload_register('autoloadCores');
spl_autoload_register('autoloadControllers');
spl_autoload_register('autoloadModels');
spl_autoload_register('autoloadLibraries');

// Start Gusto.
startup();