<?php 

/**
 * Title
 */
$_['title'] = 'Clients';

/**
 * Description
 */
$_['description'] = '';

/**
 * Alerts
 */
$_['name_invalid'] = 'Names must be letters A-Z.';
$_['name_empty'] = 'Please enter a name.';
$_['phone_empty'] = 'Please enter a phone number.';
$_['email_taken'] = 'The email address you have chosen is already taken.';
$_['email_invalid'] = 'The email address you entered was invalid.';
$_['email_empty'] = 'Please enter an email address.';
$_['pw_weak'] = 'The password is too weak. <br><b>Passwords must:</b> <br>- be 8 or more characters <br>- Have at least 1 number. <br>- Have at least 1 upper case letter. <br>- Contain one of the following (!@#$%).';
$_['pw_match'] = 'The passwords you entered did not match. ';
$_['pw_empty'] = 'Please enter a password and repeat the password to confirm.';
$_['signup_success'] = 'Your account has been created and an activation email has been sent to you.';
$_['signup_fail'] = 'Unable to create your account. Please contact an administrator if the proplem persists.';
$_['user_banned'] = 'This account has been permanently banned.';
$_['activate_mail_fail'] = 'Your account was created but we were unable to send an activation email. Please contact an administrator if the problem persists.';

/**
 * Logs
 */
$_['signup_attempt'] = 'A sign up attempted was made using the email ( {{email}} ). Reason for failure:';